package test;

import junit.framework.TestCase;
import model.data.structures.Queue;

public class QueueTest extends TestCase{
	
	private Queue<String> fila;
	
	private void setupEscenario1()
	{
		fila = new Queue<String>();
	}

	private void setupEscenario2()
	{
		fila.enqueue("nombre");
		fila.enqueue("apellido");
		fila.enqueue("cosa");
		fila.enqueue("item");
		fila.enqueue("nada");
	}
	
	public void testFila()
	{
		setupEscenario1();
		
		assertEquals("El numero de elementos deberia ser 0",0,fila.size());
		assertTrue("La fila deberia ser vac�a", fila.isEmpty());
		assertEquals("No deberia tener elementos", null, fila.dequeue());
		
		setupEscenario2();
		
		assertEquals("El numero de elementos deberia ser 5", 5, fila.size());
		assertFalse("La fila no deberia ser vac�a", fila.isEmpty());
		assertEquals("El elemento deberia ser 'nombre' ", "nombre", fila.dequeue());
		assertEquals("El elemento deberia ser 'apellido'", "apellido", fila.dequeue());
		fila.enqueue("animal");
		assertEquals("El elemento deberia ser 'cosa'", "cosa", fila.dequeue());
	}
}
