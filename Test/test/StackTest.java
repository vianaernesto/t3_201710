package test;

import junit.framework.TestCase;
import model.data.structures.Stack;

public class StackTest extends TestCase {
	
	private Stack<String> pila;

	
	private void setupEscenario1()
	{
		pila  = new Stack<String>();
		
	}
	
	private void setupEscenario2()
	{
		pila.push("nombre");
		pila.push("apellido");
		pila.push("cosa");
		pila.push("item");
		pila.push("nada");
	}
	
	public void testPila()
	{
		setupEscenario1();
		assertEquals("El tama�o de la pila deberia ser 0", 0, pila.size());
		assertNull("No deberia haber elementos", pila.pop());
		assertEquals("No deberia haber elemento en el top", null, pila.obtenerItemTop());
		assertTrue("No deberia tener elementos", pila.isEmpty());
		
		setupEscenario2();
		
		assertEquals("El tama�o deberia ser 5", 5, pila.size());
		assertFalse("Deberia tener elementos", pila.isEmpty());
		assertEquals("Deberia sacar el elemento del top", "nada", pila.pop());
		assertEquals("El elemento deberia ser 'item' ", "item", pila.obtenerItemTop());
		assertEquals("El elemento deberia ser 'item'","item", pila.pop());
		assertEquals("El elemento deberia ser 'cosa'", "cosa", pila.pop());
		pila.push("animal");
		assertEquals("El elemento deberia ser 'animal'", "animal", pila.pop());		
	}

}
