package controller;

import model.data.structures.Queue;
import model.data.structures.Stack;
import model.logic.*;

public class Controller {
	
	public static Operaciones operaciones = new Operaciones();
	
	public static boolean expresionBienFormada(String expresion)
	{
		return operaciones.expresionBienFormada(expresion);
	}
	
	public static Queue<String> ordenarPila()
	{
		return operaciones.ordenarPila();
	}

}
