package model.logic;

import model.data.structures.*;

public class Operaciones {


	private Stack<String> pila; 
	
	public Operaciones()
	{
		pila = new Stack<String>();
	}
	
	public boolean expresionBienFormada(String expresion)
	{
		
		
		
		for(int i = 0; i < expresion.length();)
		{
			String exp = expresion.substring(i, i+1);
			pila.push(exp);
			return true;
		}
		return false;
		
	}

	public Queue<String> ordenarPila()
	{
		String operadores =  "";
		String numeros = "";
		String parentesis = "";
		String corch = "";
		Queue<String> fila = new Queue<String>();
		for(int i = 0; i < pila.size(); i++)
		{
			if(pila.pop().equals(""))
				continue;
			
			if(pila.pop().equals("(") || pila.pop().equals(")"))
			{
				
				parentesis = parentesis + pila.pop() + ",";
			}
			
			else if(pila.pop().equals("+") || pila.pop().equals("-") ||pila.pop().equals("*") || pila.pop().equals("/"))
			{
				operadores = operadores + pila.pop() + ",";
			}
			else if(pila.pop().equals("[") || pila.pop().equals("]"))
			{
				corch = corch + pila.pop() + ",";
			}
			else if (Integer.parseInt(pila.pop()) >= '0' && Integer.parseInt(pila.pop()) <= '9')
			{
				numeros = numeros + pila.pop() + ",";
			}
			
		}
		
		String num[] = numeros.split(",");
		String parent[] = parentesis.split(",");
		String oper[] = operadores.split(",");
		String cor[] = corch.split(",");
		for(int j= 0; j < num.length; j++)
		{
			fila.enqueue(num[j]);
		}
		for(int j= 0; j < oper.length; j++)
		{
			fila.enqueue(oper[j]);
		}
		for(int j= 0; j < parent.length; j++)
		{
			fila.enqueue(parent[j]);
		}
		for(int j= 0; j < cor.length; j++)
		{
			fila.enqueue(cor[j]);
		}
		
		return fila;
		
		
		
	}

}
