package model.data.structures;


public class NodoDoble<T> {

	private T elem;

	
	private NodoDoble<T> siguiente;

	
	private NodoDoble<T> anterior; 

	
	public NodoDoble(){
		this.elem = null;
		this.siguiente = null;
		this.anterior = null;
	}

	
	public NodoDoble(T elem, NodoDoble<T> siguiente, NodoDoble<T> anterior){
		super();

		this.elem = elem;
		this.siguiente = siguiente;
		this.anterior = anterior;

	}

	
	public T darElem()
	{
		return elem;
	}

	
	public void establecerElem( T elem)
	{
		this.elem = elem;
	}

	
	public NodoDoble<T> darSiguiente(){
		return siguiente;
	}

	
	public NodoDoble<T> darAnterior(){
		return anterior;
	}
	
	
	public void establecerSiguiente(NodoDoble<T> Siguiente)
	{
		this.siguiente = Siguiente;
	}
	
	
	public void establecerAnterior(NodoDoble<T> Anterior)
	{
		this.anterior = Anterior;
	}

}
