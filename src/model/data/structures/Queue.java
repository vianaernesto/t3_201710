package model.data.structures;

public class Queue <T>{

	private ListaDobleEncadenada<T> lista;
	
	private int longitud;
	
	public Queue()
	{
		this.lista = new ListaDobleEncadenada<T>();
		this.longitud = 0;
	}
	
	public T dequeue()
	{
		T item  = null;
		if(!isEmpty())
		{
			item = lista.darPrimero().darElem();
			lista.eliminarPrimerElemento();
			longitud--;
			
		}
		return item;
	}
	
	public void enqueue(T item)
	{
		lista.agregarElementoFinal(item);
		longitud++;
	}
	public boolean isEmpty()
	{
		return lista.estaVacio();
	}
	
	public int size()
	{
		return longitud;
	}
}
