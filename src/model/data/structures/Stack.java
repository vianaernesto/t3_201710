package model.data.structures;

public class Stack <T>{
	
	private ListaDobleEncadenada<T> lista;
	
	private int longitud;
	

	public Stack()
	{
		this.lista = new ListaDobleEncadenada<T>();
		this.longitud = 0;
		
	}
	
	public T obtenerItemTop()
	{
		if(isEmpty()){
			return null;
		}
		
		return lista.darPrimero().darElem();
	}
		
	
	public boolean isEmpty()
	{
		return lista.estaVacio();
	}
	
	public int size()
	{
		return longitud;
	}
	
	public void push(T item)
	{
		lista.agregarElementoPrincipio(item);
		longitud++;
	}
	
	public T pop()
	{
		T item = null;
		
		if(!isEmpty())
		{
			item = lista.darPrimero().darElem();
			lista.eliminarPrimerElemento();
			longitud--;
			
		}
		return item;
	}
}
